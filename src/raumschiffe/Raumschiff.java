package raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

    private String name;
    private int statatusEnergieversorgung;
    private int statusSchutzschilde;
    private int statusLebenserhaltungssysteme;
    private int statusHuelle;
    private int anzahlPhotonentorpedos;
    private int anzahlReparaturandroiden;
    public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    public ArrayList<Ladung> ladungsliste = new ArrayList<Ladung>();

    //Parameterloser Konstruktor

    /**
     * Parameterloser Konstruktor für die Klasse Raumschiff
     */
    public Raumschiff() {

    }

    //Parametrierter Konstruktor

    /**
     * Parametrierter Konstruktor für die Klasse Raumschiff
     * @param name Name des Schiffs
     * @param statusEnergieversorgung Zustand der Energieversorgung in %
     * @param statusSchutzschilde Zustand der Schutzschilde in %
     * @param statusLebenserhaltungssysteme Zustand der Lebenserhaltung in %
     * @param statusHuelle Zustand der Schiffshuelle in %
     * @param anzahlPhotonentorpedos Anzahl der Photonentorpedos
     * @param anzahlReparaturandroide Anzahl der Reparaturandroiden
     */
    public Raumschiff(String name, int statusEnergieversorgung, int statusSchutzschilde, int statusLebenserhaltungssysteme, int statusHuelle, int anzahlPhotonentorpedos, int anzahlReparaturandroide) {
        this.name = name;
        this.statatusEnergieversorgung = statusEnergieversorgung;
        this.statusSchutzschilde = statusSchutzschilde;
        this.statusLebenserhaltungssysteme = statusLebenserhaltungssysteme;
        this.statusHuelle = statusHuelle;
        this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
        this.anzahlReparaturandroiden = anzahlReparaturandroide;
    }

    //Setter und Getter Methoden

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return statatusEnergieversorgung
     */
    public int getStatatusEnergieversorgung() {
        return statatusEnergieversorgung;
    }

    /**
     *
     * @param statatusEnergieversorgung
     */
    public void setStatatusEnergieversorgung(int statatusEnergieversorgung) {
        this.statatusEnergieversorgung = statatusEnergieversorgung;
    }

    /**
     *
     * @return statusSchutzschilde
     */
    public int getStatusSchutzschilde() {
        return statusSchutzschilde;
    }

    /**
     *
     * @param statusSchutzschilde
     */
    public void setStatusSchutzschilde(int statusSchutzschilde) {
        this.statusSchutzschilde = statusSchutzschilde;
    }

    /**
     *
     * @return statusLebenserhaltungssysteme
     */
    public int getStatusLebenserhaltungssysteme() {
        return statusLebenserhaltungssysteme;
    }

    /**
     *
     * @param statusLebenserhaltungssysteme
     */
    public void setStatusLebenserhaltungssysteme(int statusLebenserhaltungssysteme) {
        this.statusLebenserhaltungssysteme = statusLebenserhaltungssysteme;
    }

    /**
     *
     * @return statusHuelle
     */
    public int getStatusHuelle() {
        return statusHuelle;
    }

    /**
     *
     * @param statusHuelle
     */
    public void setStatusHuelle(int statusHuelle) {
        this.statusHuelle = statusHuelle;
    }

    /**
     *
     * @return anzahlPhotonentorpedos
     */
    public int getAnzahlPhotonentorpedos() {
        return anzahlPhotonentorpedos;
    }

    /**
     *
     * @param anzahlPhotonentorpedos
     */
    public void setAnzahlPhotonentorpedos(int anzahlPhotonentorpedos) {
        this.anzahlPhotonentorpedos = anzahlPhotonentorpedos;
    }

    /**
     *
     * @return anzahlReparaturandroiden
     */
    public int getAnzahlReparaturandroiden() {
        return anzahlReparaturandroiden;
    }

    /**
     *
     * @param anzahlReparaturandroiden
     */
    public void setAnzahlReparaturandroiden(int anzahlReparaturandroiden) {
        this.anzahlReparaturandroiden = anzahlReparaturandroiden;
    }

    //Ladung hinzufügen

    /**
     * Es gibt die Möglichkeit das Raumschiff mit einer Ladung zu beladen.
     * Die öffentliche Methode bekommt die aufzuladende Ladung übergeben und
     * die Ladung wird in die Arraylist ladungsliste des Raumschiffes geschrieben.
     * @param neueLadung Die Ladung ,mit der das Raumschiff beladen wird.
     */
    public void beladen(Ladung neueLadung) {
        ladungsliste.add(neueLadung);
    }

    //Der Zustand des Raumschiffes ausgegeben

    /**
     * Alle Zustände (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen ausgeben.
     */

    public void zustandsberichtAusgeben() {
        System.out.println("Zustand des Schiffs " + this.getName() + ":");
        System.out.println("Energieversorgung: " + this.getStatatusEnergieversorgung() + "%");
        System.out.println("Schutzschilde: " + this.getStatusSchutzschilde() + "%");
        System.out.println("Lebenserhaltungssysteme: " + this.getStatusLebenserhaltungssysteme() + "%");
        System.out.println("Huelle: " + this.getStatusHuelle() + "%");
        System.out.println("Photonentorpedos: " + this.getAnzahlPhotonentorpedos());
        System.out.println("Reparaturdroiden: " + this.getAnzahlReparaturandroiden());
    }

    //Beginn Methoden

    //Ladungsverzeichnis ausgeben

    /**
     * Alle Ladungen eines Raumschiffes auf der Konsole mit Ladungsbezeichnung und Menge ausgeben.
     */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Das Schiff " + this.getName() + " hat folgende Ladung: ");
        for (Ladung i : ladungsliste) {
            System.out.println(i.getBezeichnung() + ": " + i.getMenge());
        }
    }

    //Photonentorpedos abschießen

    /**
     * Das Raumschiff kann Photonentorpedos abschießen.
     * Die öffentliche Methode bekommt ein Raumschiff übergeben, dass das Ziel ist.
     * Sind keine Torpedos geladen, so wird via der broadcast Methode die Nachricht an Alle -=*Click*=- ausgegeben.
     * Sonst wird die Torpedoanzahl um 1 reduziert und
     * via der broadcast Methode die Nachricht an Alle “Photonentorpedo abgeschossen” gesendet.
     * Außerdem wird die Methode trefferEinstecken mit dem getroffenen Raumschiff als Argument aufgerufen.
     * Ist das Raumschiff, das Ziel des Angriffs ist, bereits zerstört, wird stattdessen
     * die Nachricht "Befehl nicht ausführbar, [Schiffsname] bereits zerstört." ausgegeben.
     * @param ptZiel Raumschiff, das Ziel des Torpedos ist.
     */
    public void feuernPhotonentorpedos(Raumschiff ptZiel) {
        if (ptZiel.statusLebenserhaltungssysteme > 0) {
            if (this.anzahlPhotonentorpedos <= 0) {
                this.broadcast(this.getName() + ": -=*Click*=-");
            } else {
                this.anzahlPhotonentorpedos--;
                this.broadcast(this.getName() + ": Photonentorpedo abgeschossen");
                trefferEinstecken(ptZiel);
            }
        } else {
            System.out.println("Befehl nicht ausführbar, " + ptZiel.name + " bereits zerstört.");
        }
    }

    //Phaserkanone abschießen

    /**
     * Das Raumschiff kann Phaserkanonen abschießen.
     * Die öffentliche Methode bekommt ein Raumschiff übergeben, dass das Ziel ist.
     * Ist die Energieversorgung kleiner als 50%, so wird via der broadcast Methode eine Nachricht an Alle -=*Click*=- ausgegeben.
     * Ansonsten wird die Energieversorgung um 50% reduziert und via der broadcast Methode die Nachricht an Alle “Phaserkanone abgeschossen” gesendet.
     * Außerdem wird die Methode trefferEinstecken mit dem getroffenen Raumschiff als Argument aufgerufen.
     * Ist das Raumschiff, das Ziel des Angriffs ist, bereits zerstört, wird stattdessen
     * die Nachricht "Befehl nicht ausführbar, [Schiffsname] bereits zerstört." ausgegeben.
     * @param pkZiel Das Raumschiff auf das mit den Phaserkanonen geschossen wird.
     */
    public void feuernPhaserkanonen(Raumschiff pkZiel) {
        if (pkZiel.statusLebenserhaltungssysteme > 0) {
            if (this.statatusEnergieversorgung < 50) {
                this.broadcast(this.getName() + ": -=*Click*=-");
            } else {
                this.statatusEnergieversorgung -= 50;
                this.broadcast(this.getName() + ": Phaserkanone abgeschossen");
                trefferEinstecken(pkZiel);
            }
        } else {
            System.out.println("Befehl nicht ausführbar, " + pkZiel.name + " bereits zerstört.");
        }
    }

    //Einen Treffer einstecken

    /**
     * Ein Raumschiff kann von verschiedenen Waffen getroffen werden.
     * Dabei wird eine nicht öffentliche Methode aufgerufen, die vermerkt,
     * ob das Raumschiff getroffen wird und das Ausmaß des Treffers berechnet.
     * Die Schilde des getroffenen Raumschiffs werden um 50% geschwächt.
     * Sollte anschließend die Schilde vollständig zerstört worden sein,
     * so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
     * Sollte danach der Zustand der Hülle auf 0% absinken, so sind die Lebenserhaltungssysteme vollständig zerstört
     * und es wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind,
     * indem die Methode broadcast mit der Nachricht "Die Lebenserhaltungssysteme der [Schiffsname] wurden vernichtet")
     * aufgerufen wird.
     * @param getroffen Das getroffene Raumschiff
     */
    private void trefferEinstecken(Raumschiff getroffen) {
        getroffen.statusSchutzschilde -= 50;
        getroffen.broadcast(getroffen.getName() + " wurde getroffen!");

        if (getroffen.statusSchutzschilde <= 0) {
            getroffen.statusSchutzschilde = 0;
            getroffen.statusHuelle -= 50;
            getroffen.statatusEnergieversorgung -= 50;
        }
        if (getroffen.statatusEnergieversorgung <= 0) {
            getroffen.statatusEnergieversorgung = 0;
        }
        if (getroffen.statusHuelle <= 0) {
            getroffen.statusHuelle = 0;
            getroffen.statusLebenserhaltungssysteme = 0;
            getroffen.broadcast("Die Lebenserhaltungssysteme der " + getroffen.getName() + " wurden vernichtet");
        }
    }

    //Die Nachricht wird dem broadcastKommunikator hinzugefügt und in der Konsole ausgegeben

    /**
     * Es können von einem Raumschiff Nachrichten an Alle gesendet werden.
     * Dafür muss die Nachricht selbst an die Methode übergeben werden.
     * @param nachricht Die zur Arraylist broadcastKommunikator hinzuzufügende Nachricht.
     */
    public void broadcast(String nachricht) {
        broadcastKommunikator.add(nachricht);
        System.out.println(nachricht);
    }

    //Logbucheinträge zurückgeben

    /**
     * Die Klasse Raumschiff hat eine öffentliche, statische Methode,
     * die alle Einträge des Logbuchs aller Raumschiffe in Form einer Liste zurückgibt.
     * @return broadcastKommunikator Der Braodcastkommuniktor in den alle Logbucheinträge geschrieben werden.
     */
    public static ArrayList<String> logbuchAusgeben() {
        return broadcastKommunikator;
    }

    // Ladung “Photonentorpedos” einsetzen

    /**
     * Sollte als Ladung “Photonentorpedos” vorhanden sein, so können diese in die Torpedorohre des Raumschiffs geladen werden.
     * Die öffentliche Methode bekommt die Anzahl der Torpedos als Parameter übergeben.
     * Gibt es keine Ladung Photonentorpedos auf dem Schiff, wird als Nachricht Keine Photonentorpedos gefunden!
     * in der Konsole ausgegeben und die Nachricht an alle -=*Click*=- ausgegeben.
     * Ist die Anzahl der einzusetzenden Photonentorpedos größer als die Menge der tatsächlich Vorhandenen,
     * so werden alle vorhandenen Photonentorpedos eingesetzt.
     * Ansonsten wird die Ladungsmenge Photonentorpedos über die Setter Methode vermindert und die Anzahl der Photonentorpedo im Raumschiff erhöht.
     * Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben.
     * @param torpedoAnzahl Anzahl der zu ladenden Torpedos
     */
    public void torpedosLaden(int torpedoAnzahl) {
        int menge;
        boolean torpedoGefunden = false;

        for (Ladung i : ladungsliste) {
            if (i.getBezeichnung().equals("Photonentorpedo") && i.getMenge() > 0) {
                menge = i.getMenge();
                torpedoGefunden = true;
                if (torpedoAnzahl >= menge) {
                    torpedoAnzahl = menge;
                }
                this.setAnzahlPhotonentorpedos(this.getAnzahlPhotonentorpedos() + torpedoAnzahl);
                i.setMenge(i.getMenge() - torpedoAnzahl);
                System.out.println(this.getName() + ": " + torpedoAnzahl + " Photonentorpedo(s) eingesetzt");
            }
        }
        if (!torpedoGefunden) {
            System.out.println(this.getName() +": " + "Keine Photonentorpedos gefunden!");
            this.broadcast(this.getName() + ": -=*Click*=-");
        }
    }

    //Ladungsverzeichnis aufräumen

    /**
     * Das Ladungsverzeichnis wird durchsucht und jede Ladung mit der Menge 0 aus dem Ladungsverzeichnis entfernt.
     * Es wird eine Nachricht ausgegeben, wenn eine Ladung entfernt wird: "Ladung [Ladungsbezeichnung] mit der Menge 0, wurde aus dem Verzeichnis entfernt."
     */
    public void ladungsverzeichnisAufraeumen() {
        for (int i = 0; i < ladungsliste.size(); i++) {
            if (ladungsliste.get(i).getMenge() <= 0) {
                System.out.println(this.getName() +": Ladung " + "'" + ladungsliste.get(i).getBezeichnung() + "' mit der Menge 0, wurde aus dem Verzeichnis entfernt.");
                ladungsliste.remove(i);
                i = i - 1;
            }
        }
    }

    //Reparatur-Androiden einsetzen

    /**
     *Einen Reparaturauftrag für Schilde, Hülle oder Energieversorgung an ein oder mehreren Reparatur-Androiden senden.
     * Die öffentliche Methode bekommt drei boolesche Werte für Schutzschilde, Energieversorgung und Schiffshülle übergeben
     * sowie die Anzahl der eingesetzten Androiden.
     *Die Methode entscheidet anhand der übergebenen Parameter, welche Schiffsstrukturen repariert werden sollen.
     * Es wird eine Zufallszahl zwischen 0 - 100 erzeugt, welche für die Berechnung der Reparatur benötigt wird.
     * Ist die übergebene Anzahl von Androiden größer als die vorhandene Anzahl von Androiden im Raumschiff,
     * dann wird die vorhandene Anzahl von Androiden eingesetzt.
     * Prozentuale Berechnung der reparierten Schiffsstrukturen: (Zufallszahl * Anzahl der eingesetzten Druiden) / Anzahl der auf true gesetzten Schiffssturkturen
     * Das Ergebnis wird den auf true gesetzten Schiffsstrukturen hinzugefügt.
     * @param schutzschilde Schutzschulde: Wenn true, wird repariert, wenn false, wird nicht repariert
     * @param energieversorgung Energieversorgung: Wenn true, wird repariert, wenn false, wird nicht repariert
     * @param huelle Schiffshuelle: Wenn true, wird repariert, wenn false, wird nicht repariert
     * @param anzahlEinsatzdruiden Anzahl der engesetzten Reparaturdroiden
     */
    public void reparatur(boolean schutzschilde, boolean energieversorgung, boolean huelle, int anzahlEinsatzdruiden) {
        int anzahlStrukturen = 0;
        int zufallszahl = (int) (1 + Math.random() * 100);

        if (!(this.anzahlReparaturandroiden <= 0)) {
            //System.out.println("zufallszahl: " + zufallszahl);
            if (schutzschilde) {
                anzahlStrukturen++;
            }
            if (energieversorgung) {
                anzahlStrukturen++;
            }
            if (huelle) {
                anzahlStrukturen++;
            }
            //System.out.println("anzahl strukturen: " + anzahlStrukturen);
            if (anzahlEinsatzdruiden > this.anzahlReparaturandroiden) {
                anzahlEinsatzdruiden = this.anzahlReparaturandroiden;
            }
            //System.out.println("anzahl druiden: " + anzahlEinsatzdruiden);
            int reparaturwert = zufallszahl * anzahlEinsatzdruiden / anzahlStrukturen;
            //System.out.println("reparaturwert: " + reparaturwert);
            if (schutzschilde) {
                this.setStatusSchutzschilde(this.getStatusSchutzschilde() + reparaturwert);
            }
            if (energieversorgung) {
                this.setStatatusEnergieversorgung(this.getStatatusEnergieversorgung() + reparaturwert);
            }
            if (huelle) {
                this.setStatusHuelle(this.getStatusHuelle() + reparaturwert);
            }
            this.setAnzahlReparaturandroiden(this.getAnzahlReparaturandroiden() - anzahlEinsatzdruiden);
        } else System.out.println(this.getName() + ": " + "Reparatur nicht möglich, keine Reparaturdroiden vorhanden.");
    }

    //Ende Methoden
}