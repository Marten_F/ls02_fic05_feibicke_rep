package raumschiffe;

public class Ladung {

    private String bezeichnung;
    private int menge;

    /**
     * Parameterloser Konstruktor für die Klasse Ladung
     */
    public Ladung() {

    }

    /**
     * Parametrierter Konstruktor für die Klasse Ladung
     *
     * @param bezeichnung Bezeichnung der Ladung
     * @param menge       Menge der Ladung
     */
    public Ladung(String bezeichnung, int menge) {

        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    /**
     * @return bezeichnung
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * @param bezeichnung
     */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    /**
     * @return menge
     */
    public int getMenge() {
        return menge;
    }

    /**
     * @param menge
     */
    public void setMenge(int menge) {
        this.menge = menge;
    }
}
