package raumschiffe;

public class Main {
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 100, 100, 100, 100, 0, 5);

        Ladung ladungKling1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung ladungKling2 = new Ladung("Bat'leth Klingonen Schwert", 200);
        Ladung ladungRom1 = new Ladung("Borg-Schrott", 5);
        Ladung ladungRom2 = new Ladung("Rote Materie", 2);
        Ladung ladungRom3 = new Ladung("Plasma-Waffe", 50);
        Ladung ladungVul1 = new Ladung("Forschungssonde", 35);
        Ladung ladungVul2 = new Ladung("Photonentorpedo", 3);

        klingonen.beladen(ladungKling1);
        klingonen.beladen(ladungKling2);
        romulaner.beladen(ladungRom1);
        romulaner.beladen(ladungRom2);
        romulaner.beladen(ladungRom3);
        vulkanier.beladen(ladungVul1);
        vulkanier.beladen(ladungVul2);

        klingonen.feuernPhotonentorpedos(romulaner);
        romulaner.feuernPhaserkanonen(klingonen);
        vulkanier.broadcast("Gewalt ist nicht logisch");
        klingonen.zustandsberichtAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reparatur(true, true, true, vulkanier.getAnzahlReparaturandroiden());
        vulkanier.torpedosLaden(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.feuernPhotonentorpedos(romulaner);
        klingonen.feuernPhotonentorpedos(romulaner);

        klingonen.zustandsberichtAusgeben();
        klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandsberichtAusgeben();
        romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandsberichtAusgeben();
        vulkanier.ladungsverzeichnisAusgeben();
    }
}
